# Lamp XML Parser

Convert the Lamp XML format to a MOT-like format, with extra fields

`<frame>, <id>, <bb_left>, <bb_top>, <bb_width>, <bb_height>, <bb_class>, <left-signal>, <brake>, <right-signal>, <side>, <image_name>, <occluded>`
 
 | field | type | description |
 | --- | --- | --- |
 | frame | int | Frame ID, sequential ID | 
 | id | int | Object Track ID |
 | bb_left | int | X coord of the box |
 | bb_top | int | Y coord of the box |
 | bb_width | int | Width of the box |
 | bb_height | int | Height of the box |
 | bb_class | string | Object Class |
 | left-signal | bool | Left Winker state |
 | brake | bool | Brake Light state |
 | left-signal | bool | Right Winker state |
 | side | string | {Front, Rear} view of the vehicle |
 | image_name | string | Frame ID with .jpg extension | 
 | occluded| bool | Defines if the obstacle is occluded | 
 
## How to use

```
python lamp_xml_parser.py [-o OUTFILE] [-d DISPLAY] xmlfile
xmlfile Path to the Lamp XML file format.
[-o OUTFILE] Path to text file, if given the parsed data will be stored in CSV format, as defined above.
[-d DISPLAY] Path to the images, if given the images and the rectangles will be displayed
```

## Example

`python lamp_xml_parser.py /home/user/data/file.xml`

This command parses, and show the track ID of all the lamps in the terminal.

Based on the work shared in https://github.com/flo-gehring/CVATXmlParser