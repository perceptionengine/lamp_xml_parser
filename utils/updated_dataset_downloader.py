import os
import shutil
import urllib3
import argparse
from bs4 import BeautifulSoup


class Downloader:
    def __init__(self, url, output, username, password, current_dir):
        self.url = url
        self.output= output
        self.headers = urllib3.make_headers(basic_auth=username + ":" + password)
        self.current_annotations = os.listdir(current_dir)
        self.run()

    def run(self):
        http = urllib3.PoolManager()
        r = http.request("GET", self.url, headers=self.headers)
        soup = BeautifulSoup(r.data, "html.parser")
        files_list = []
        for e in soup.find_all("a"):
            name = e.text
            name = name.replace(" ", "")
            if "zip" in name:
                files_list.append(name)

        for file_name in files_list:
            basename = file_name.split(".")[0]
            if not self.already_downloaded(basename):
                filename = os.path.join(self.output, file_name)
                file_url = os.path.join(self.url, file_name)
                with http.request("GET", file_url, headers=self.headers, preload_content=False) as res, open(
                    filename, "wb"
                ) as out_file:
                    shutil.copyfileobj(res, out_file)

    def already_downloaded(self, new_file_basename):
        for current_annotation in self.current_annotations:
            if new_file_basename in current_annotation:
                return True
        return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Downloader", formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-i", "--input", required=True, help="input download url")
    parser.add_argument("-o", "--output", required=True, help="output path")
    parser.add_argument("--username", help="url username")
    parser.add_argument("--password", help="url password")
    parser.add_argument("--current_dir", help="current annotation directory")
    args = parser.parse_args()
    downloader = Downloader(args.input, args.output, args.username, args.password, args.current_dir)
