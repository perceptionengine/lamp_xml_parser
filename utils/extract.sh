#!/bin/bash
TARGET_DIR=$1/../images
mkdir $TARGET_DIR

for FILE_NAME in `ls $1 | grep zip | xargs`; do
    TARGET_FILE=$1/$FILE_NAME
    EXTRACT_DIR=$TARGET_DIR/`basename $TARGET_FILE .zip`
    unzip $TARGET_FILE -d $EXTRACT_DIR
done
